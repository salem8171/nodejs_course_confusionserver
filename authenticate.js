const passport = require('passport');
const LocalStrategie = require('passport-local').Strategy;
const JwtStrategie = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const jwt = require('jsonwebtoken');

const config = require('./config');

var Users = require('./models/users');

exports.local = passport.use(new LocalStrategie(Users.authenticate()));
passport.serializeUser(Users.serializeUser());
passport.deserializeUser(Users.deserializeUser());

exports.getToken = (user) => {
    return jwt.sign(user, config.secretKey, { expiresIn: 3600 });
}

var options = {};
options.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
options.secretOrKey = config.secretKey;

exports.jwtPassport = passport.use(new JwtStrategie(options, (jwt_payload, done) => {
    console.log("JWT payload", jwt_payload);

    Users.findOne({ _id: jwt_payload._id }, (error, user) => {
        if (error) {
            return done(error, false);
        } else if (user)
        {
            return done(null, user);
        } else
        {
            return done(null, false);
        }
    });
}));

exports.verifyUser = passport.authenticate('jwt', { session: false });
exports.verifyAdmin = (request, respnse, next) => {
    if (request.user.admin == true)
    {
        return next();
    } else 
    {
        var error = new Error("You are not authorised to perfrom this action!");
        error.status = 401;
        return next(error);
    }
}