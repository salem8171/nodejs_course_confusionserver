var express = require('express');
var promotionRouter = express.Router();
const authenticate = require('../authenticate');

const Promotions = require('../models/promotions');

promotionRouter.route('/')
.get((request, response, next) => {
  Promotions.find({})
  .then((promotions) => {
    response.statusCode = 200;
    response.setHeader('Content-Type', 'application/json');
    response.json(promotions);
  }, (error) => next(error))
  .catch((error) => next(error));
})
.post(authenticate.verifyUser, authenticate.verifyAdmin, (request, response, next) => {
  Promotions.create(request.body)
  .then((promotion) => {
    response.statusCode = 200;
    response.setHeader('Content-Type', 'application/json');
    response.json(promotion);
  }, (error) => next(error))
  .catch((error) => next(error));
})
.put(authenticate.verifyUser, authenticate.verifyAdmin, (request, response, next) => {
  response.statusCode = 403;
  response.setHeader('Content-Type', 'text/plain');
  response.end('PUT operation not supported on /promotions\n');
})
.delete(authenticate.verifyUser, authenticate.verifyAdmin, (request, response, next) => {
  Promotions.remove({})
  .then((resp) => {
    response.statusCode = 200;
    response.setHeader('Content-Type', 'application/json');
    response.json(resp);
  }, (error) => next(error))
  .catch((error) => next(error));
});

promotionRouter.route('/:promotionId')
.get((request, response, next) => {
  Promotions.findById(request.params.promotionId)
  .then((promotion) => {
    response.statusCode = 200;
    response.setHeader('Content-Type', 'application/json');
    response.json(promotion);
  }, (error) => next(error))
  .catch((error) => next(error));
})
.post(authenticate.verifyUser, authenticate.verifyAdmin, (request, response, next) => {
  response.statusCode = 403;
  response.setHeader('Content-Type', 'text/plain');
  response.end('POST operation not supported on /promotions/' + request.params.promotionId + '\n');
})
.put(authenticate.verifyUser, authenticate.verifyAdmin, (request, response, next) => {
  Promotions.findByIdAndUpdate(request.params.promotionId, { $set: request.body }, { new: true })
  .then((promotion) => {
    response.statusCode = 200;
    response.setHeader('Content-Type', 'application/json');
    response.json(promotion);
  }, (error) => next(error))
  .catch((error) => next(error));
})
.delete(authenticate.verifyUser, authenticate.verifyAdmin, (request, response, next) => {
  Promotions.findByIdAndRemove(request.params.promotionId)
  .then((resp) => {
    response.statusCode = 200;
    response.setHeader('Content-Type', 'application/json');
    response.json(resp);
  }, (error) => next(error))
  .catch((error) => next(error));
});

module.exports = promotionRouter;
