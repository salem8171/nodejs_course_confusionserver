var express = require('express');
var leaderRouter = express.Router();
const authenticate = require('../authenticate');

const Leaders = require('../models/leaders');

leaderRouter.route('/')
.get((request, response, next) => {
  Leaders.find({})
  .then((Leaders) => {
    response.statusCode = 200;
    response.setHeader('Content-Type', 'application/json');
    response.json(Leaders);
  }, (error) => next(error))
  .catch((error) => next(error));
})
.post(authenticate.verifyUser, authenticate.verifyAdmin, (request, response, next) => {
  Leaders.create(request.body)
  .then((leader) => {
    response.statusCode = 200;
    response.setHeader('Content-Type', 'application/json');
    response.json(leader);
  }, (error) => next(error))
  .catch((error) => next(error));
})
.put(authenticate.verifyUser, authenticate.verifyAdmin, (request, response, next) => {
  response.statusCode = 403;
  response.setHeader('Content-Type', 'text/plain');
  response.end('PUT operation not supported on /leaders\n');
})
.delete(authenticate.verifyUser, authenticate.verifyAdmin, (request, response, next) => {
  Leaders.remove({})
  .then((resp) => {
    response.statusCode = 200;
    response.setHeader('Content-Type', 'application/json');
    response.json(resp);
  }, (error) => next(error))
  .catch((error) => next(error));
});

leaderRouter.route('/:leaderId')
.get((request, response, next) => {
  Leaders.findById(request.params.leaderId)
  .then((leader) => {
    response.statusCode = 200;
    response.setHeader('Content-Type', 'application/json');
    response.json(leader);
  }, (error) => next(error))
  .catch((error) => next(error));
})
.post(authenticate.verifyUser, authenticate.verifyAdmin, (request, response, next) => {
  response.statusCode = 403;
  response.setHeader('Content-Type', 'text/plain');
  response.end('POST operation not supported on /leaders/' + request.params.leaderId + '\n');
})
.put(authenticate.verifyUser, authenticate.verifyAdmin, (request, response, next) => {
  Leaders.findByIdAndUpdate(request.params.leaderId, { $set: request.body }, { new: true })
  .then((leader) => {
    response.statusCode = 200;
    response.setHeader('Content-Type', 'application/json');
    response.json(leader);
  }, (error) => next(error))
  .catch((error) => next(error));
})
.delete(authenticate.verifyUser, authenticate.verifyAdmin, (request, response, next) => {
  Leaders.findByIdAndRemove(request.params.leaderId)
  .then((resp) => {
    response.statusCode = 200;
    response.setHeader('Content-Type', 'application/json');
    response.json(resp);
  }, (error) => next(error))
  .catch((error) => next(error));
});

module.exports = leaderRouter;
