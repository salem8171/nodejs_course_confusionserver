var express = require('express');
var router = express.Router();
const passport = require('passport');
const authenticate = require('../authenticate');

var Users = require('../models/users');

/* GET users listing. */
router.get('/', authenticate.verifyUser, authenticate.verifyAdmin, function(request, response, next) {
  Users.find({})
  .then((users) => {
    response.statusCode = 200;
    response.setHeader('Content-Type', 'application/json');
    response.json(users);
  }, (error) => next(error))
  .catch((error) => next(error));
});

router.post('/signup', (request, response, next) => {
  Users.register(new Users({ username: request.body.username }), request.body.password, (error, user) => {
    if (error)
    {
      response.statusCode = 500;
      response.setHeader('Content-Type', 'application/json');
      response.json({ error: error });
    } else 
    {
      if (request.body.firstname)
      {
        user.firstname = request.body.firstname;
      }
      if (request.body.lastname)
      {
        user.lastname = request.body.lastname;
      }
      user.save((error, user) => {
        if (error)
        {
          response.statusCode = 500;
          response.setHeader('Content-Type', 'application/json');
          response.json({ error: error });
        } else
        {
          passport.authenticate('local')(request, response, () => {
            response.statusCode = 200;
            response.setHeader('Content-Type', 'application/json');
            response.json({ success: true, status: 'Registration success!' });
          });
        }
      });
    }
  });
});

router.post('/login', passport.authenticate('local'), (request, response) => {
  var token = authenticate.getToken({ _id: request.user._id });
  response.statusCode = 200;
  response.setHeader('Content-Type', 'application/json');
  response.json({ success: true, token: token, status: 'Logged in successfully!' });
});

router.get('/logout', (request, response, next) => {
  if (request.session)
  {
    request.session.destroy();
    response.clearCookie('session-id');
    response.redirect('/');
  } else
  {
    var error = new Error('You are not logged in!');
    error.status = 403;
    next(error);
  }
});

module.exports = router;
