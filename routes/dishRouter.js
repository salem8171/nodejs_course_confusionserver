var express = require('express');
var dishRouter = express.Router();
const authenticate = require('../authenticate');

const Dishes = require('../models/dishes');

dishRouter.route('/')
.get((request, response, next) => {
  Dishes.find({})
  .populate('comments.author')
  .then((dishes) => {
    response.statuscode = 200;
    response.setHeader('Content-Type', 'application/json');
    response.json(dishes);
  }, (error) => next(error))
  .catch((error) => next(error));
})
.post(authenticate.verifyUser, authenticate.verifyAdmin, (request, response, next) => {
  Dishes.create(request.body)
  .then((dish) => {
    response.statuscode = 200;
    response.setHeader('Content-Type', 'application/json');
    response.json(dish);
  }, (error) => next(error))
  .catch((error) => next(error));
})
.put(authenticate.verifyUser, authenticate.verifyAdmin, (request, response, next) => {
  response.statusCode = 403;
  response.setHeader('Content-Type', 'text/plain');
  response.end('PUT operation not supported on /dishes\n');
})
.delete(authenticate.verifyUser, authenticate.verifyAdmin, (request, response, next) => {
  Dishes.remove({})
  .then((resp) => {
    response.statuscode = 200;
    response.setHeader('Content-type', 'application/json');
    response.json(resp);
  }, (error) => next(error))
  .catch((error) => next(error));
});

dishRouter.route('/:dishId')
.get((request, response, next) => {
  Dishes.findById(request.params.dishId)
  .populate('comments.author')
  .then((dish) => {
    response.statuscode = 200;
    response.setHeader('Content-Type', 'application/json');
    response.json(dish);
  }, (error) => next(error))
  .catch((error) => next(error));
})
.post(authenticate.verifyUser, authenticate.verifyAdmin, (request, response, next) => {
  response.statusCode = 403;
  response.setHeader('Content-Type', 'text/plain');
  response.end('POST operation not supported on /dishes/' + request.params.dishId + '\n');
})
.put(authenticate.verifyUser, authenticate.verifyAdmin, (request, response, next) => {
  Dishes.findByIdAndUpdate(request.params.dishId, { $set: request.body }, { new: true })
  .then((dish) => {
    response.statuscode = 200;
    response.setHeader('Content-Type', 'application/json');
    response.json(dish);
  }, (error) => next(error))
  .catch((error) => next(error));
})
.delete(authenticate.verifyUser, authenticate.verifyAdmin, (request, response, next) => {
  Dishes.findByIdAndRemove(request.params.dishId)
  .then((resp) => {
    response.statuscode = 200;
    response.setHeader('Content-type', 'application/json');
    response.json(resp);
  }, (error) => next(error))
  .catch((error) => next(error));
});

dishRouter.route('/:dishId/comments')
.get((request, response, next) => {
  Dishes.findById(request.params.dishId)
  .populate('comments.author')
  .then((dish) => {
    if (dish != null)
    {
      response.statuscode = 200;
      response.setHeader('Content-Type', 'application/json');
      response.json(dish.comments);
    } else 
    {
      error = new Error('Dish: ' + request.params.dishId + ' was not found');
      error.status = 404;
      next(error);
    }
  }, (error) => next(error))
  .catch((error) => next(error));
})
.post(authenticate.verifyUser, (request, response, next) => {
  Dishes.findById(request.params.dishId)
  .then((dish) => {
    if (dish != null)
    {
      request.body.author = request.user._id;
      dish.comments.push(request.body);
      dish.save()
      .then((dish) => {
        response.statuscode = 200;
        response.setHeader('Content-Type', 'application/json');
        response.json(dish);
      }, (error) => next(error));
    } else 
    {
      error = new Error('Dish: ' + request.params.dishId + ' was not found');
      error.status = 404;
      next(error);
    }
  }, (error) => next(error))
  .catch((error) => next(error));
})
.put(authenticate.verifyUser, (request, response, next) => {
  response.statusCode = 403;
  response.setHeader('Content-Type', 'text/plain');
  response.end('PUT operation not supported on /dishes/' + request.params.dishId + '/comments\n');
})
.delete(authenticate.verifyUser, authenticate.verifyAdmin, (request, response, next) => {
  Dishes.findById(request.params.dishId)
  .then((dish) => {
    if (dish != null)
    {
      for ( var i = dish.comments.length - 1; i >= 0; i--)
      {
        dish.comments.id(dish.comments[i]._id).remove();
      }
      dish.save()
      .then((dish) => {
        response.statuscode = 200;
        response.setHeader('Content-Type', 'application/json');
        response.json(dish);
      }, (error) => next(error));
    } else 
    {
      error = new Error('Dish: ' + request.params.dishId + ' was not found');
      error.status = 404;
      next(error);
    }
  }, (error) => next(error))
  .catch((error) => next(error));
});

dishRouter.route('/:dishId/comments/:commentId')
.get((request, response, next) => {
  Dishes.findById(request.params.dishId)
  .populate('comments.author')
  .then((dish) => {
    if (dish != null && dish.comments.id(request.params.commentId) != null)
    {
      response.statuscode = 200;
      response.setHeader('Content-Type', 'application/json');
      response.json(dish.comments.id(dish.comments.id(request.params.commentId)));
    } else if (dish == null)
    {
      error = new Error('Dish: ' + request.params.dishId + ' was not found');
      error.status = 404;
      next(error);
    } else 
    {
      error = new Error('Comment: ' + request.params.commentId + ' was not found');
      error.status = 404;
      next(error);
    }
  }, (error) => next(error))
  .catch((error) => next(error));
})
.post(authenticate.verifyUser, (request, response, next) => {
  response.statusCode = 403;
  response.setHeader('Content-Type', 'text/plain');
  response.end('POST operation not supported on /dishes/' + request.params.dishId + '/comments/' + request.params.commentId + '\n');
})
.put(authenticate.verifyUser, (request, response, next) => {
  Dishes.findById(request.params.dishId)
  .then((dish) => {
    if (dish != null && dish.comments.id(request.params.commentId) != null && dish.comments.id(request.params.commentId).author.equals(request.user._id))
    {
      if (request.body.rating)
      {
        dish.comments.id(request.params.commentId).rating = request.body.rating;
      }
      if (request.body.comment)
      {
        dish.comments.id(request.params.commentId).comment = request.body.comment;
      }
      dish.save()
      .then((dish) => {
        response.statuscode = 200;
        response.setHeader('Content-Type', 'application/json');
        response.json(dish);
      }, (error) => next(error));
    } else if (dish == null)
    {
      error = new Error('Dish: ' + request.params.dishId + ' was not found');
      error.status = 404;
      next(error);
    } else if (dish.comments.id(request.params.commentId) == null)
    {
      error = new Error('Comment: ' + request.params.commentId + ' was not found');
      error.status = 404;
      next(error);
    } else 
    {
      error = new Error("You are not authorised to perform this action!");
      error.status = 401;
      next(error);
    }
  }, (error) => next(error))
  .catch((error) => next(error));
})
.delete(authenticate.verifyUser, (request, response, next) => {
  Dishes.findById(request.params.dishId)
  .then((dish) => {
    if (dish != null && dish.comments.id(request.params.commentId) != null && dish.comments.id(request.params.commentId).author.equals(request.user._id))
    {
      dish.comments.id(request.params.commentId).remove();
      dish.save()
      .then((dish) => {
        response.statuscode = 200;
        response.setHeader('Content-Type', 'application/json');
        response.json(dish);
      }, (error) => next(error));
    } else if (dish == null) 
    {
      error = new Error('Dish: ' + request.params.dishId + ' was not found');
      error.status = 404;
      next(error);
    } else if (dish.comments.id(request.params.commentId) == null)
    {
      error = new Error('Comment: ' + request.params.commentId + ' was not found');
      error.status = 404;
      next(error);
    } else
    {
      error = new Error("You are not authorised to perform this action!");
      error.status = 401;
      next(error);
    }
  }, (error) => next(error))
  .catch((error) => next(error));
});

module.exports = dishRouter;
